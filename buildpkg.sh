#!/usr/bin/env bash

set -e

DEBIAN_IMAGE="${DEBIAN_IMAGE:-debian}"
DEBIAN_TAG="${DEBIAN_TAG:-bullseye}"
FLAVOUR="${FLAVOUR:-openssh84}"

if [ -z "${GITLAB_CI}" ] || [ "$1" == docker ]; then
  docker build -t "pkgbuilder-${DEBIAN_IMAGE}:${DEBIAN_TAG}" - <<DOCKER
FROM ${DEBIAN_IMAGE}:${DEBIAN_TAG}

ENV DEBIAN_FRONTEND=noninteractive
RUN sed -i 'p; s/^deb /deb-src /' /etc/apt/sources.list
RUN sort -Vu /etc/apt/sources.list
RUN apt update
RUN apt install -y build-essential autotools-dev debhelper dh-autoreconf dh-exec pkg-config zlib1g-dev sudo devscripts
RUN apt build-dep -y ssh
# libaudit-dev libedit-dev libgtk-3-dev libkrb5-dev libpam0g-dev libselinux1-dev libssl-dev libsystemd-dev libwrap0-dev
RUN apt dist-upgrade -y
DOCKER

  cleanup () {
    docker kill "pkgbuilder-${DEBIAN_IMAGE}-${DEBIAN_TAG}-${FLAVOUR}" &>/dev/null || true
  }

  trap cleanup INT
  if [ "$1" == docker ]; then
    exit 0
  fi
fi

builder () {
  if [ -z "${GITLAB_CI}" ]; then
    docker run --name "pkgbuilder-${DEBIAN_IMAGE}-${DEBIAN_TAG}-${FLAVOUR}" --rm -i --net=host --workdir "$(pwd)" -v "$(pwd):$(pwd)" "pkgbuilder-${DEBIAN_IMAGE}:${DEBIAN_TAG}" bash
  else
    bash
  fi
}

get_source () {
  builder <<EOF
    set -xe
    if test -d src; then
      echo 'sources exist! create-patch and clean before trying again.'
      exit 1
    fi
    mkdir src
    cd src
    apt update
    apt source ssh=${PATCH_PKG_VER:?}
    cd openssh*/
    patch -p1 < ../../debian.patch
    patch -p1 < ../../${FLAVOUR}_libproxyproto_compat.patch
    chown -R $UID ..
EOF
}

create_patch_from_change () {
  builder <<EOF &
    set -xe
    cd src/openssh*/
    export EDITOR=true
    rm *.orig
    DEBFULLNAME="GitLab Core-Infra" DEBEMAIL="ops-contact@gitlab.com" dch --local "+gitlab+${FLAVOUR}+${CI_COMMIT_SHORT_SHA:-local}+" "Creating patch for libproxyproto compatibility (version: ${CI_COMMIT_SHORT_SHA:-local})"
    dpkg-source --commit . ${FLAVOUR}_libproxyproto_compat.patch
    cp debian/patches/${FLAVOUR}_libproxyproto_compat.patch ../../new.patch
    chown $UID ../../new.patch
EOF
}

build_packages () {
  builder <<EOF
    cd src/openssh*/
    cp -rv ../../libproxyproto .
    rm *.orig
    export EDITOR=true
    DEBFULLNAME="GitLab Core-Infra" DEBEMAIL="ops-contact@gitlab.com" dch --local "+gitlab+${FLAVOUR}+${CI_COMMIT_SHORT_SHA:-local}+" "Adding libproxyproto (version: ${CI_COMMIT_SHORT_SHA:-local})"
    dpkg-source --commit . ${FLAVOUR}_libproxyproto.patch
EOF
  builder <<EOF &
    cd src/openssh*/
    dpkg-buildpackage -us -uc
EOF
  wait
  builder <<EOF
    cd src/openssh*/
    dpkg -i ../*.deb || true
    apt install -yf
    dpkg -l | grep openssh | grep gitlab
    chown -R $UID ..
EOF
  mkdir -p "pkg/${DEBIAN_IMAGE}/${DEBIAN_TAG}/${FLAVOUR}/"
  find src/ -maxdepth 1 -type f | grep "${FLAVOUR}" | xargs -rn1 -I% cp -v % "pkg/${DEBIAN_IMAGE}/${DEBIAN_TAG}/${FLAVOUR}/"
}

if [ "$1" == "build" ]; then
  get_source
  build_packages
  exit $?
fi

if [ "$1" == "prepare" ]; then
  get_source
  exit $?
fi

if [ "$1" == "create-patch" ]; then
  if [ -f new.patch ]; then
    echo 'refusing to create a new.patch while already present.'
    exit 1
  fi
  create_patch_from_change
  exit $?
fi

if [ "$1" == "clean" ]; then
  echo rm -rf src/ | builder
  exit $?
fi

cat <<EOF
PATCH_PKG_VER=XYZ $0 build|prepare|create-patch
  build:        fetch the source package for openssh, apply the patch, and build new packages.
  prepare:      fetch the source package for openssh and apply the patch. This is to do work on the patch itself.
  create-patch: create a new.patch in this directory, containing any changes to the source (except changes in debian/). The resulting patch is to be renamed to ${FLAVOUR}_libproxyproto_compat.patch.
  clean:        delete current sources and packages.

Current DEBIAN_IMAGE: ${DEBIAN_IMAGE}
Current DEBIAN_TAG: ${DEBIAN_TAG}
Current PATCH_PKG_VER: ${PATCH_PKG_VER:-unset}
Current FLAVOUR: ${FLAVOUR}

Example:

PATCH_PKG_VER=1:8.4p1-5 $0 build
EOF

exit 1
