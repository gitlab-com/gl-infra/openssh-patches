This patch supports OpenSSH 8.4+
--- /dev/null
+++ openssh-8.4p1/Makefile.custom
@@ -0,0 +1,38 @@
+.PHONY: sshd all tests openssh_tests clean rebase libproxyproto
+sshd all &:: Makefile
+	$(MAKE) -f Makefile -w $@
+
+libproxyproto:
+	$(MAKE) -w -C libproxyproto all
+
+libproxyproto/libproxyproto.a:
+	$(MAKE) -w -C libproxyproto libproxyproto.a
+
+libproxyproto/libproxyproto_connect.so:
+	$(MAKE) -w -C libproxyproto libproxyproto_connect.so
+
+Makefile: configure libproxyproto/libproxyproto.a
+	./configure --with-pam --with-kerberos5 --with-selinux --with-security-key-builtin '--with-ldflags=libproxyproto/libproxyproto.a' '--with-cflags=-DLINK_LIBPROXYPROTO=1'
+
+configure:
+	autoreconf
+
+clean:
+	-$(MAKE) -f Makefile -w clean
+	-$(MAKE) -f Makefile -w -C libproxyproto clean
+	-rm -f compile_commands.json Makefile configure
+
+.PHONY: compile_commands.json
+compile_commands.json:
+	$(MAKE) -f Makefile.custom -w -d all libproxyproto 2>/dev/null | compiledb --full-path -f -o compile_commands.json
+
+rebase: clean
+	git remote add openssh https://github.com/openssh/openssh-portable.git || git remote set-url openssh https://github.com/openssh/openssh-portable.git
+	git fetch -u openssh master:openssh
+	git rebase openssh master
+
+tests: libproxyproto/libproxyproto_connect.so regress/ssh_config regress/sshd_config
+	LIBPROXYPROTO_DEBUG=1 ./custom_tests.sh
+
+openssh_tests regress/ssh_config regress/sshd_config &:: Makefile
+	TEST_SSH_UNSAFE_PERMISSIONS=1 LIBPROXYPROTO_DEBUG=1 $(MAKE) -f Makefile -w tests
--- openssh-8.4p1.orig/canohost.c
+++ openssh-8.4p1/canohost.c
@@ -158,7 +158,7 @@ ipv64_normalise_mapped(struct sockaddr_s
  * The returned string must be freed.
  */
 static char *
-get_socket_address(int sock, int remote, int flags)
+get_socket_address(int sock, int remote, int flags, struct sockaddr_storage *existing)
 {
 	struct sockaddr_storage addr;
 	socklen_t addrlen;
@@ -170,8 +170,12 @@ get_socket_address(int sock, int remote,
 	memset(&addr, 0, sizeof(addr));

 	if (remote) {
-		if (getpeername(sock, (struct sockaddr *)&addr, &addrlen) != 0)
-			return NULL;
+		if (existing != NULL) {
+			memcpy(&addr, existing, sizeof(addr));
+		} else {
+			if (getpeername(sock, (struct sockaddr *)&addr, &addrlen) != 0)
+				return NULL;
+		}
 	} else {
 		if (getsockname(sock, (struct sockaddr *)&addr, &addrlen) != 0)
 			return NULL;
@@ -204,11 +208,18 @@ get_socket_address(int sock, int remote,
 }

 char *
-get_peer_ipaddr(int sock)
+get_peer_ipaddr(int sock, struct sockaddr_storage *existing, int force_no_env)
 {
 	char *p;

-	if ((p = get_socket_address(sock, 1, NI_NUMERICHOST)) != NULL)
+	if (force_no_env == 0) {
+		char *proxy_ipaddr = getenv("OPENSSH_PROXY_HOST");
+		if (proxy_ipaddr) {
+			return xstrdup(proxy_ipaddr);
+		}
+	}
+
+	if ((p = get_socket_address(sock, 1, NI_NUMERICHOST, existing)) != NULL)
 		return p;
 	return xstrdup("UNKNOWN");
 }
@@ -218,7 +229,7 @@ get_local_ipaddr(int sock)
 {
 	char *p;

-	if ((p = get_socket_address(sock, 0, NI_NUMERICHOST)) != NULL)
+	if ((p = get_socket_address(sock, 0, NI_NUMERICHOST, NULL)) != NULL)
 		return p;
 	return xstrdup("UNKNOWN");
 }
@@ -229,7 +240,7 @@ get_local_name(int fd)
 	char *host, myname[NI_MAXHOST];

 	/* Assume we were passed a socket */
-	if ((host = get_socket_address(fd, 0, NI_NAMEREQD)) != NULL)
+	if ((host = get_socket_address(fd, 0, NI_NAMEREQD, NULL)) != NULL)
 		return host;

 	/* Handle the case where we were passed a pipe */
@@ -246,7 +257,7 @@ get_local_name(int fd)
 /* Returns the local/remote port for the socket. */

 static int
-get_sock_port(int sock, int local)
+get_sock_port(int sock, int local, struct sockaddr_storage *existing)
 {
 	struct sockaddr_storage from;
 	socklen_t fromlen;
@@ -262,10 +273,14 @@ get_sock_port(int sock, int local)
 			return 0;
 		}
 	} else {
-		if (getpeername(sock, (struct sockaddr *)&from, &fromlen) == -1) {
-			debug("getpeername failed: %.100s", strerror(errno));
-			return -1;
-		}
+		if (existing != NULL) {
+			memcpy(&from, existing, sizeof(from));
+		} else {
+			if (getpeername(sock, (struct sockaddr *)&from, &fromlen) == -1) {
+				debug("getpeername failed: %.100s", strerror(errno));
+				return -1;
+			}
+   }
 	}

 	/* Work around Linux IPv6 weirdness */
@@ -285,13 +300,22 @@ get_sock_port(int sock, int local)
 }

 int
-get_peer_port(int sock)
+get_peer_port(int sock, struct sockaddr_storage *existing, int force_no_env)
 {
-	return get_sock_port(sock, 0);
+	char *proxy_port = getenv("OPENSSH_PROXY_PORT");
+	int port;
+	if (proxy_port && force_no_env == 0) {
+		if (sscanf(proxy_port, "%d", &port) == 1) {
+			debug("got port from env: %d", port);
+			return port;
+		}
+	}
+
+	return get_sock_port(sock, 0, existing);
 }

 int
 get_local_port(int sock)
 {
-	return get_sock_port(sock, 1);
+	return get_sock_port(sock, 1, NULL);
 }
--- openssh-8.4p1.orig/canohost.h
+++ openssh-8.4p1/canohost.h
@@ -18,8 +18,8 @@
 struct ssh;

 char		*remote_hostname(struct ssh *);
-char		*get_peer_ipaddr(int);
-int		 get_peer_port(int);
+char		*get_peer_ipaddr(int, struct sockaddr_storage*, int);
+int		get_peer_port(int, struct sockaddr_storage*, int);
 char		*get_local_ipaddr(int);
 char		*get_local_name(int);
 int		get_local_port(int);
--- openssh-8.4p1.orig/channels.c
+++ openssh-8.4p1/channels.c
@@ -1685,8 +1685,8 @@ channel_post_x11_listener(struct ssh *ss
 		return;
 	}
 	set_nodelay(newsock);
-	remote_ipaddr = get_peer_ipaddr(newsock);
-	remote_port = get_peer_port(newsock);
+	remote_ipaddr = get_peer_ipaddr(newsock, NULL, 0);
+	remote_port = get_peer_port(newsock, NULL, 0);
 	snprintf(buf, sizeof buf, "X11 connection from %.200s port %d",
 	    remote_ipaddr, remote_port);

@@ -1709,8 +1709,8 @@ port_open_helper(struct ssh *ssh, Channe
 {
 	char *local_ipaddr = get_local_ipaddr(c->sock);
 	int local_port = c->sock == -1 ? 65536 : get_local_port(c->sock);
-	char *remote_ipaddr = get_peer_ipaddr(c->sock);
-	int remote_port = get_peer_port(c->sock);
+	char *remote_ipaddr = get_peer_ipaddr(c->sock, NULL, 0);
+	int remote_port = get_peer_port(c->sock, NULL, 0);
 	int r;

 	if (remote_port == -1) {
--- /dev/null
+++ openssh-8.4p1/custom_tests.sh
@@ -0,0 +1,25 @@
+#!/usr/bin/env bash
+set -xeufo pipefail
+
+CODE=1
+
+cleanup () {
+  kill -9 "${SSH_PID}"
+  exit $CODE
+}
+
+trap cleanup EXIT
+
+LIBPROXYPROTO_MUST_USE_PROTOCOL_HEADER=1 LIBPROXYPROTO_DEBUG=1 $(pwd)/sshd -f $(pwd)/regress/sshd_config -o ListenAddress=::1 -o AddressFamily=any -De &
+SSH_PID=$!
+echo "started SSHd: ${SSH_PID}"
+sleep 3
+
+echo IPv6 address
+LD_PRELOAD=libproxyproto/libproxyproto_connect.so LIBPROXYPROTO_ADDR=dead::beef $(pwd)/ssh -6 -o ControlMaster=no -F $(pwd)/regress/ssh_config -o Hostname=::1 localhost env | tee /proc/self/fd/2 | grep -A 99 -B 99 '^SSH_CONNECTION=dead::beef 8080'
+
+echo IPv4 address
+LD_PRELOAD=libproxyproto/libproxyproto_connect.so LIBPROXYPROTO_ADDR=8.8.8.8 $(pwd)/ssh -4 -o ControlMaster=no -F $(pwd)/regress/ssh_config -o Hostname=127.0.0.1 localhost env | tee /proc/self/fd/2 | grep -A 99 -B 99 '^SSH_CONNECTION=8.8.8.8 8080'
+
+echo "passed"
+CODE=0
--- openssh-8.4p1.orig/packet.c
+++ openssh-8.4p1/packet.c
@@ -532,8 +532,8 @@ ssh_remote_ipaddr(struct ssh *ssh)
 	if (ssh->remote_ipaddr == NULL) {
 		if (ssh_packet_connection_is_on_socket(ssh)) {
 			sock = ssh->state->connection_in;
-			ssh->remote_ipaddr = get_peer_ipaddr(sock);
-			ssh->remote_port = get_peer_port(sock);
+			ssh->remote_ipaddr = get_peer_ipaddr(sock, NULL, 0);
+			ssh->remote_port = get_peer_port(sock, NULL, 0);
 			ssh->local_ipaddr = get_local_ipaddr(sock);
 			ssh->local_port = get_local_port(sock);
 		} else {
--- openssh-8.4p1.orig/sshd.c
+++ openssh-8.4p1/sshd.c
@@ -135,6 +135,10 @@ int allow_severity;
 int deny_severity;
 #endif /* LIBWRAP */

+#ifdef LINK_LIBPROXYPROTO
+#include "libproxyproto/libproxyproto.h"
+#endif
+
 /* Re-exec fds */
 #define REEXEC_DEVCRYPTO_RESERVED_FD	(STDERR_FILENO + 1)
 #define REEXEC_STARTUP_PIPE_FD		(STDERR_FILENO + 2)
@@ -908,9 +912,9 @@ drop_connection(int sock, int startups)
 	ndropped++;

 	laddr = get_local_ipaddr(sock);
-	raddr = get_peer_ipaddr(sock);
+	raddr = get_peer_ipaddr(sock, NULL, 0);
 	do_log2(drop_level, "drop connection #%d from [%s]:%d on [%s]:%d "
-	    "past MaxStartups", startups, raddr, get_peer_port(sock),
+	    "past MaxStartups", startups, raddr, get_peer_port(sock, NULL, 0),
 	    laddr, get_local_port(sock));
 	free(laddr);
 	free(raddr);
@@ -1302,6 +1306,24 @@ server_accept_loop(int *sock_in, int *so
 					break;
 				}

+			char *proxy_ipaddr;
+			char *proxy_port;
+
+			if (*newsock != -1) {
+				{
+					proxy_ipaddr = get_peer_ipaddr(*newsock, &from, 1);
+					debug("PROXY_HOST: %s", proxy_ipaddr);
+					setenv("OPENSSH_PROXY_HOST", proxy_ipaddr, 1);
+					debug("OPENSSH_PROXY_HOST: %s", proxy_ipaddr);
+				}
+				{
+					proxy_port = malloc(16);
+					snprintf(proxy_port, 16, "%d", get_peer_port(*newsock, &from, 1));
+					setenv("OPENSSH_PROXY_PORT", proxy_port, 1);
+					debug("OPENSSH_PROXY_PORT: %s", proxy_port);
+				}
+			}
+
 			/*
 			 * Got connection.  Fork a child to handle it, unless
 			 * we are in debugging mode.
@@ -1554,6 +1576,10 @@ main(int ac, char **av)
 	Authctxt *authctxt;
 	struct connection_info *connection_info = NULL;

+#ifdef LINK_LIBPROXYPROTO
+	proxyproto_init();
+#endif
+
 #ifdef HAVE_SECUREWARE
 	(void)set_auth_parameters(ac, av);
 #endif
