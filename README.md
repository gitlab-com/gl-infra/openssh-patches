# OpenSSH patches

A patchset to enable supplemental features in OpenSSH for GitLab.com (SasS)

### Patches

| Patch | What does it do? |
| ----- | -----------------|
| `debian.patch` | Incorporates changes to the Debian build-hooks to update `CFLAGS` and `LDFLAGS` of OpenSSH as well as including a hook to compile the bundled `libproxyproto`.
| `opensshXY_libproxyproto_compat.patch` | Incorporates changes (to the respective OpenSSH version X.Y+) to enable support for libproxyproto.

#### How does the compatibility-layer work?

When OpenSSH receives a new connection, the main process will `accept()` this connection. In the case of this patchset, this function-call is hooked by `libproxyproto`. `libproxyproto` will inspect the incoming connection for a header matching the `PROXY`-protocol, and if applicable update the peer addresses of the passed `sockaddr` struct.

OpenSSH will take this address as the source of the connection. However, because OpenSSH will fork the connection into a new process, this information on the `sockaddr` struct will be lost. Thus this patchset sets the environment variables `OPENSSH_PROXY_HOST` and `OPENSSH_PROXY_PORT`. This environment will be forked off with the client connection. Because OpenSSH is single-threaded, this will cause a 1:1 relation for a set of environment variables and a client connection.

The forked off process will then on each invocation of `get_peer_ipaddr` and `get_peer_port` check these `OPENSSH_PROXY_HOST` and `OPENSSH_PROXY_PORT` variables, respectively.  
If the variable holds a value, this will be used instead of the actual address on the file descriptor (which would in the case of GitLab.com always contain the IP of on of our loadbalancers).

### How to make changes to these patches

1. Install [Quilt](https://wiki.debian.org/UsingQuilt).

1. Set up your [`.quiltrc` configuration](https://wiki.debian.org/UsingQuilt#Using_.quiltrc_configuration_file).

1. Download the original `openssh` source package and the Debian or Ubuntu packages.
For example, Debian Buster's version can be found in
https://packages.debian.org/buster/openssh-server.  There will be two
`.tar.gz` files: one is the original OpenSSH source, and the other holds
the Debian patches and build scripts.

    ```sh
    wget http://deb.debian.org/debian/pool/main/o/openssh/openssh_7.9p1.orig.tar.gz
    wget http://deb.debian.org/debian/pool/main/o/openssh/openssh_7.9p1-10+deb10u2.debian.tar.xz

1. Extract the original source archive:

    ```sh
    tar xzvf openssh_7.9p1.orig.tar.gz
    ```

1. Extract the `.debian.tar.xz` inside the directory:

    ```sh
    cd openssh-7.9p1
    tar xvf ../openssh_7.9p1-10+deb10u2.debian.tar.xz
    ```

1. Apply all the `debian/patches` to the source:

    ```sh
    quilt push -a
    ```

1. Import the custom patches:

    ```
    quilt import ../openssh79_libproxyproto_compat.patch
    quilt push
    ```

1.  This should bring in the `libproxyproto` changes and apply them. If you need to make
    changes to a file, run `quilt add <filename>`, edit the file, and then run `quilt refresh`.
    The updated patch will be located in `debian/patches/openssh79_libproxyproto_compat.patch`.

### License

- OpenSSH: [BSD (or more permissive)](https://github.com/openssh/openssh-portable/blob/d3cc4d650ce3e59f3e370b101778b0e8f1c02c4d/LICENCE)
- libproxyproto: [ISC](https://gitlab.com/gitlab-com/gl-infra/openssh-patches/-/blob/master/libproxyproto/LICENSE)
- patches in this repository: Same license as OpenSSH where applicable, otherwise donated to Public Domain.

This patchset is a derivative work of https://github.com/T4cC0re/openssh-portable and https://github.com/T4cC0re/libproxyproto.   Permission to inherit the modifications was granted.
